interface ICustomer{
    id: string;
    name: string;
    username: string;
    email: string;
    address: string;
    phone: string;
    website: string;
    company: string;
    avatar: string;

}

export class Main{

    constructor(){
        const button = document.getElementById("populate") as HTMLButtonElement;
         button.onclick = () => this.populateData();
        // button.addEventListener("click",() => {
        //     this.populateDate();
        //     });
        }


    public async populateData(){
        let tableBody = document.getElementById("table-body");
        let url = "https://cs4350.herokuapp.com/demo/all";
        let customerData = await this.loadData(url);
        tableBody.innerHTML = this.createHTMLFromArray(customerData);
        console.log("Populate data");
    }

    public async loadData(url: string): Promise<ICustomer[]>{
        const responce= await fetch(url);
        const json = await responce.json();
        return json as ICustomer[];

        // return fetch(url).then(responce => {
        //     return responce. json();
        // })
        
    }

    public createHTMLFromArray(dataArray: ICustomer[]): string{
        let listHtml = "";
        let template = document.getElementById("table-template");
        let templateHtml = template.innerHTML;

       for(let index =0; index< dataArray.length; index++){
           listHtml += templateHtml
           .replace(/{{id}}/g, dataArray[index].id)
           .replace(/{{name}}/g, dataArray[index].name)
           .replace(/{{username}}/g, dataArray[index].username)
           .replace(/{{email}}/g, dataArray[index].email)
           .replace(/{{address}}/g, dataArray[index].address)
           .replace(/{{phone}}/g, dataArray[index].phone)
           .replace(/{{website}}/g, dataArray[index].website)
           .replace(/{{company}}/g, dataArray[index].company)
           .replace(/{{avatar}}/g, dataArray[index].avatar)
       }
        return listHtml;
    } 
        
}

new Main();