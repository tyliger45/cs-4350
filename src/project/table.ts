// script that rus and fetches the json data
var url = 'https://raw.githubusercontent.com/tyliger45/webprogramming/a77c729da509d184a3122406c91ef6c6aa823ee7/info.json'
var ul = document.getElementById('authors');

function createNode(element) {
    return document.createElement(element); // Create the type of element you pass in the parameters
}

function append(parent, el) {
    return parent.appendChild(el);
}

fetch(url)
    .then(function (response) {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
                response.status);
            return;
        }
        response.json().then(function (data) {
            return data.map(function (data) {
                let l1 = createNode('td');
                let l2 = createNode('td');
                let l3 = createNode('td');
                let l4 = createNode('td');
                let tr = createNode('tr');
                var span = createNode('span');
                var age = createNode('age');
                var phone = createNode('phone');
                var email = createNode('a');
                span.innerHTML = `${data.firstName} ${data.lastName}`;
                age.innerHTML = `${data.age}`;
                phone.innerHTML = `${data.phoneNumbers}`;
                email.innerHTML = `${data.email}`;
                email.href = 'mailto:' + `${data.email}`;
                append(ul, tr);
                append(l1, span);
                append(l2, age);
                append(l3, phone);
                append(l4, email);
                append(tr, l1);
                append(tr, l2);
                append(tr, l3);
                append(tr, l4);
                

            })
        })

    })

    .catch(function (err) {
        console.log('Fetch Error :-S', err);
    });