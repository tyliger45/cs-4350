// makes the three.js sceen and all code needed to run

(function () {
  var scene = new THREE.Scene();
  scene.background = new THREE.Color(0x000);
  var div = document.getElementsByClassName("area2")[0];

  var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);


  var renderer = new THREE.WebGLRenderer();
  renderer.setSize(div.clientWidth, div.clientWidth)
  scene.add(camera);
  div.appendChild(renderer.domElement);
  // adds the snow effect change flakecount to make run better 
  var flakeCount = 10000;
  var flakeGeometry = new THREE.TetrahedronGeometry(0.035);
  var flakeMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
  var snow = new THREE.Group();
  var i;
  for (i = 0; i < flakeCount; i++) {
    var flakeMesh = new THREE.Mesh(flakeGeometry, flakeMaterial);
    flakeMesh.position.set(
      (Math.random() - 0.5) * 40,
      (Math.random() - 0.5) * 20,
      (Math.random() - 0.5) * 40
    );
    snow.add(flakeMesh);
  }
  scene.add(snow);

  var flakeArray = snow.children;

  var geometry = new THREE.BoxGeometry(1, 1, 1);
  var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
  var cube1 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0x3236a8 });
  var cube2 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0x22d4c8 });
  var cube3 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0x19520a });
  var cube4 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0xff7700 });
  var cube5 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
  var cube6 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0xff00cc });
  var cube7 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0xeeff00 });
  var cube8 = new THREE.Mesh(geometry, material);
  var material = new THREE.MeshBasicMaterial({ color: 0xffffff });
  var cube9 = new THREE.Mesh(geometry, material);

  scene.add(cube1);
  cube1.position.x = 0;
  cube1.position.y = 0;
  cube1.position.z = 0;
  scene.add(cube2);
  cube2.position.x = 3;
  cube2.position.y = 0;
  cube2.position.z = 0;
  scene.add(cube3);
  cube3.position.x = -3;
  cube3.position.y = 0;
  cube3.position.z = 0;
  scene.add(cube4);
  cube4.position.x = 3;
  cube4.position.y = 2;
  cube4.position.z = 0;
  scene.add(cube5);
  cube5.position.x = -3;
  cube5.position.y = 2;
  cube5.position.z = 0;
  scene.add(cube6);
  cube6.position.x = 0;
  cube6.position.y = 2;
  cube6.position.z = 0;
  scene.add(cube7);
  cube7.position.x = 0;
  cube7.position.y = -2;
  cube7.position.z = 0;
  scene.add(cube8);
  cube8.position.x = 3;
  cube8.position.y = -2;
  cube8.position.z = 0;
  scene.add(cube9);
  cube9.position.x = -3;
  cube9.position.y = -2;
  cube9.position.z = 0;



  camera.position.z = 10;
  var direction = 0.01
  var animate = function () {

    requestAnimationFrame(animate);

    if (cube1.position.x > 2) {
      direction = -0.01;
    }
    if (cube1.position.x < -2) {
      direction = 0.01;
    }

    cube1.position.x += direction;
    cube4.position.x -= direction;
    cube2.position.z += direction;
    cube3.position.z -= direction;
    cube5.position.y += direction;
    cube8.position.y -= direction;
    cube7.position.y += direction;
    cube7.position.x += direction;
    cube9.position.y -= direction;
    cube9.position.z += direction;
    cube6.position.z += direction;
    cube6.position.y += direction;
    cube6.position.x += (direction + direction + direction);
    var i;

    for (i = 0; i < flakeArray.length / 2; i++) {
      flakeArray[i].rotation.y += 0.01;
      flakeArray[i].rotation.x += 0.02;
      flakeArray[i].rotation.z += 0.03;
      flakeArray[i].position.y -= 0.018;
      if (flakeArray[i].position.y < -4) {
        flakeArray[i].position.y += 10;
      }
    }

    for (i = flakeArray.length / 2; i < flakeArray.length; i++) {
      flakeArray[i].rotation.y -= 0.03;
      flakeArray[i].rotation.x -= 0.03;
      flakeArray[i].rotation.z -= 0.02;
      flakeArray[i].position.y -= 0.016;
      if (flakeArray[i].position.y < -4) {
        flakeArray[i].position.y += 9.5;
      }

      snow.rotation.y -= 0.0000002;
    }

    renderer.render(scene, camera);

  };

  animate();

})();